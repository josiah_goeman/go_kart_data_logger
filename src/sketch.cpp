#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <SD.h>
#include <stdint.h>

//sensors "flicker" a bit when changing state, creating many interrupts
//for each logical change.  logical changes can be detected by making sure
//the most recent ISR ran longer ago than this threshold period.
//100us = 1ms.  gives some wiggle room for the flickering, while still
//counting up to 1000 changes per second, well above the actual max the device will measure.
#define TRIGGER_MIN_PERIOD_MICROS 100
//if the ISR doesn't run more frequently than this threshold,
//I just call it 0RPM.  Otherwise it will just not update while not moving.
#define TRIGGER_MAX_PERIOD_MICROS 250000
//save a data point every 0.25 seconds
#define LOG_PERIOD_MILLIS 250
//gear ratio of the sprockets connected by roller chain.
//used to calculate the axle RPM from driven pulley RPM.
//will require reprogramming if either sprocket is changed.
#define JACKSHAFT_SPROCKET_TEETH 12
#define AXLE_SPROCKET_TEETH 72
//kart travels this far for each rotation of the axle.
//used to calculate the ground speed from axle rpm.  will require reprogramming
//if the wheel size changes.
#define DRIVE_WHEEL_CIRCUMFERENCE_CM 113

LiquidCrystal_I2C lcd(0x27,	//I2C address for my display.  found with https://playground.arduino.cc/Main/I2cScanner
	2,	//enable pin
	1,	//read/write pin
	0,	//register select pin
	4,	//v data pins v
	5,
	6,
	7);

//this gets set when the SD module is set up.  It's the same for the
//entire running time of the program, and different each time
//the device powers on.
char* sessionFileName = 0;

uint32_t loopCount = 0;

//variables changed from ISRs must be declared volatile so
//compiler doesn't try to do anything clever optimizing them
volatile uint32_t drivePulleyPeriod, drivenPulleyPeriod;
volatile uint32_t prevDrivePulleyTriggerTime = 0;
volatile uint32_t prevDrivenPulleyTriggerTime = 0;

//interrupt service routine for drive pulley
ISR(INT0_vect)
{
	uint32_t curTime = micros();
	uint32_t difference = curTime - prevDrivePulleyTriggerTime;

	if(difference >= TRIGGER_MIN_PERIOD_MICROS)
	{
		//average current value with older values.
		//current value is half the result, older values act to "smooth"
		//it out.  There's probably a name for this sort of integration, but I don't know it.
		drivePulleyPeriod += difference;
		drivePulleyPeriod >>= 1;

		prevDrivePulleyTriggerTime = curTime;
	}
}

//driven pulley
ISR(INT1_vect)
{
	uint32_t curTime = micros();
	uint32_t difference = curTime - prevDrivenPulleyTriggerTime;

	if(difference >= TRIGGER_MIN_PERIOD_MICROS)
	{
		drivenPulleyPeriod += difference;
		drivenPulleyPeriod >>= 1;

		prevDrivenPulleyTriggerTime = curTime;
	}
}

void setup()
{
	//for some reason beyond mortal comprehension, my lcd doesn't
	//always work on a cold start unless I call begin() more than once.
	//this is a pretty ungodly hack, but it works.
	lcd.begin(16, 2);	//16 characters, 2 lines
	lcd.begin(16, 2);	//cross fingers, 

	//turn on backlight LED
	lcd.setBacklightPin(3, POSITIVE);
	lcd.setBacklight(HIGH);

	//drive pulley interrupt is on pin 2 (INT0), driven pulley is pin 3 (INT1)
	pinMode(2, INPUT_PULLUP);
	pinMode(3, INPUT_PULLUP);

	//external interrupt control register.
	//only four lowest bits are used.
	//upper 2 of those are for INT1, lower 2 are INT0.
	//00 : LOW
	//01 : CHANGE
	//10 : FALLING
	//11 : RISING
	//I want to detect change on both INT0 and INT1, so my value is 0101
	EICRA = 0b0101;

	//external interrupt mask register.
	//two lowest bits are used.
	//whether or not to trigger ISRs for INT1 and INT0 respectively.
	//I want both, so both bits are on
	EIMSK = 0b11;

	//Serial.begin(9600);
}

//logs for each session are stored in incrementing file names.
//can store up to 999 logs on one card.
char* getNextFileName()
{
	char* fName = "log_***.csv";

	char hundreds = 0, tens = 0, ones = 0;
	while(true)
	{
		ones++;

		if(ones == 10)
		{
			tens++;
			ones = 0;
			if(tens == 10)
			{
				hundreds++;
				tens = 0;
			}
		}

		fName[4] = '0' + hundreds;	//convert digits to ascii characters
		fName[5] = '0' + tens;
		fName[6] = '0' + ones;

		if(!SD.exists(fName))
			return fName;
	}
}

//try to initialize the SD library and open a file for this loggin session.
//promp for a card to be inserted until it works
void sdInit()
{
	//default chip select pin (10 on nano) must be set as output
	//for SD library to work, even though it isn't used.
	pinMode(10, OUTPUT);

	if(SD.begin(4))	//chip select pin is 4
	{
		sessionFileName = getNextFileName();

		//create file here to check that it works.
		//if this fails, there's nothing else the software can do,
		//so it just gives up
		File sessionLog = SD.open(sessionFileName, FILE_WRITE);
		if(!sessionLog)
		{
			lcd.clear();
			lcd.print("Card corrupted");
			while(true)
				delay(1000);
		}
		sessionLog.close();

		lcd.clear();
		lcd.print(" Found SD Card! ");
		lcd.setCursor(0, 1);
		lcd.print("This is log #");
		lcd.print(sessionFileName[4]);	//hundreds
		lcd.print(sessionFileName[5]);	//tens
		lcd.print(sessionFileName[6]);	//ones
		delay(3000);
	}
	else
	{
		lcd.clear();
		lcd.print("*Insert SD Card*");
		delay(1000);
	}
}

//saves a new line to the log with millisecond timestamp
void sdLog(uint16_t drivePulleyRPM, uint16_t drivenPulleyRPM)
{
	File sessionLog = SD.open(sessionFileName, FILE_WRITE);

	sessionLog.print(millis());
	sessionLog.print(":");
	sessionLog.print(drivePulleyRPM);
	sessionLog.print(",");
	sessionLog.println(drivenPulleyRPM);
	sessionLog.close();
}

uint16_t drivenPulleyRPMToKPH(uint16_t drivenPulleyRPM)
{
	//gain 6 bits of fixed-point precision.
	//makes this a little messy, but avoids costly software floating point operations
	uint32_t fp_pulleyRPM = ((uint32_t) drivenPulleyRPM) << 6;

	uint32_t fp_axleRPM = fp_pulleyRPM * JACKSHAFT_SPROCKET_TEETH / AXLE_SPROCKET_TEETH;
	
	uint32_t fp_cmPerMinute = fp_axleRPM * DRIVE_WHEEL_CIRCUMFERENCE_CM;

	// cm/min *60 = cm/hour / 100000 = km/hour
	return (uint16_t) (((fp_cmPerMinute * 60) / 100000) >> 6);	//shed extra precision
}

uint16_t microsPeriodToRPM(uint32_t period)
{
	//sixty million microseconds per minute
	uint32_t rpm2 = (uint16_t) (60000000 / period);
	//divide by 2 because period is half a rotation.
	//white>black and black>white both trigger the sensor.
	return (uint16_t)(rpm2 >> 1);
}

void loop()
{
    if(!sessionFileName)
    {
	    sdInit();
	    return;
    }

    cli();	//disable interrupts during multi-instruction reads
    uint32_t _drivePulleyPeriod = drivePulleyPeriod;
    uint32_t _drivenPulleyPeriod = drivenPulleyPeriod;
    uint32_t _prevDrivePulleyTriggerTime = prevDrivePulleyTriggerTime;
    uint32_t _prevDrivenPulleyTriggerTime = prevDrivenPulleyTriggerTime;
    sei();	//re-enable interrupts

    uint16_t driveRPM = 0, drivenRPM = 0;
    uint32_t curTime = micros();

    uint32_t driveDifference = curTime - _prevDrivePulleyTriggerTime;
    if(driveDifference < TRIGGER_MAX_PERIOD_MICROS)
    	driveRPM = microsPeriodToRPM(_drivePulleyPeriod);

    uint32_t drivenDifference = curTime - _prevDrivenPulleyTriggerTime;
    if(drivenDifference < TRIGGER_MAX_PERIOD_MICROS)
    	drivenRPM = microsPeriodToRPM(_drivenPulleyPeriod);

    uint16_t speed = drivenPulleyRPMToKPH(drivenRPM);

    sdLog(driveRPM, drivenRPM);

	//refresh the LCD once per second (every four logs)
    if(loopCount % 4 == 0)
    {
    	lcd.clear();

    	lcd.print("RPM: ");
    	if(driveRPM < 1000) lcd.print(0);	//pad with 0's
    	if(driveRPM < 100) lcd.print(0);
    	if(driveRPM < 10) lcd.print(0);
    	lcd.print(driveRPM);

    	lcd.setCursor(0, 1);
    	lcd.print("KPH:  ");
    	if(speed < 100) lcd.print(0);
    	if(speed < 10) lcd.print(0);
    	lcd.print(speed);

    	//lcd.setCursor(15, 1);
    	//lcd.print(loopCount % 8 == 0 ? "0" : "1");
    }

    //Serial.print("RPM: ");
    //Serial.println(driveRPM);
    //Serial.print("Speed: ");
    //Serial.print(speed);
    //Serial.println("KPH");

    delay(LOG_PERIOD_MILLIS);
    loopCount++;
}
