import sys
import numpy as np
import matplotlib.pyplot as plt

file_path = sys.argv[1]
file = open(file_path, 'r')

times = []
rpms = []
speeds = []
for line in file:
	delim1 = line.find(":")
	delim2 = line.find(",")

	time = float(line[:delim1])
	time /= 1000.0	#milliseconds to seconds
	times.append(time)
	rpms.append(int(line[delim1+1:delim2]))

	speed = float(line[delim2+1:])
	speed *= 12.0 / 72	#12 pulley teeth, 72 axle sprocket teeth.  axle rpm.		
	speed *= 113		#113cm wheel circumference.  cm per minute
	speed *= 60			#minutes per hour
	speed /= 100000		#cm per km
	speeds.append(speed)

data_points = float(len(times))#100.0	#number of data points
domain = float(times[len(times)-1])#10.0		#largest number on x axis

print("data_points: " + str(data_points))
print("domain: " + str(domain))

view_window = 5*4.0	#how many points of history is shown

x = np.linspace(0, domain, data_points)

plt.style.use('fivethirtyeight')
figure, rpm_plot = plt.subplots()#figsize = (10, 10))
speed_plot = rpm_plot.twinx()

#print(plt.style.available)

for i in range(len(x)):
	plt.cla()	#clear

	rpm_plot.set_xlabel("time (s)")
	rpm_plot.set_ylabel("rpm", color = "#0000FF")
	rpm_plot.tick_params(axis = "y", colors = "#0000FF")

	speed_plot.set_ylabel("speed (kph)", color = "#FF7F00")
	speed_plot.tick_params(axis = "y", colors = "#FF7F00")

	speed_plot.set_ylabel("speed (kph)", color = "#FF7F00")
	speed_plot.tick_params(axis = "y", colors = "#FF7F00")

	rpm_plot.plot(x, rpms, color = "#0000FF")
	speed_plot.plot(x, speeds, color = "#FF7F00")

	start = ((i-view_window) / data_points) * domain
	stop = (i / data_points) * domain
	rpm_plot.axis([start, stop, 0, 5000])
	speed_plot.axis([start, stop, 0, 60])

	figure.tight_layout()

	plt.savefig('frame_%03d.png' %i)
